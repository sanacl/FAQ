## Contact Support

In case you find wrong data or you have any other question, please contact
us through the `support` repository under your organization.

Don't worry about the _assignee_ of the ticket, one of our bots is browsing
the tickets when a new request is coming so we can assing it internally. As
soon as we start working on it you'll be notified. It should only take a few hours max in working days.

E.g. [this links shows] the support tracker for the CHAOSS community

![screenshot](../images/bitergia-support-tracker.png)


[this links shows]:https://gitlab.com/Bitergia/c/CHAOSS/support
