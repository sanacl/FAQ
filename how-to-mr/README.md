# How to Merge Request to a repository

There are few steps to follow to perform a new Merge Request in a Gitlab repository. In the following documentation, you will find the steps needed for modifying an affiliations file with the [GrimoireLab](https://github.com/bitergia/identities) format.

## Generate a new branch

In the main view of the repository, click on the '+' button and select 'Create Branch':

---

![alt text](./images/branch-generation-1.png)

---

There we will provide a name for the branch. In this case, as we are modifying identities, I will call the branch `update/user-affiliation`:

---

![alt text](./images/branch-generation-2.png)

---

## Modify the files

Once the branch is generated, make sure that the selected branch in the navbar is the new one, and select the file we want to edit. There, we will find a `edit` button, and we will click on it:

---

![alt text](./images/edit-file.png)

---

Clicking there will prompt us a text editor where we can modify the file:

---

![alt text](./images/unmodified-identities.png)

---

In the image above, three unmerged identities are displayed. What we will do here is, merge manually the important information of the three identities into one. After the proper modifications it will looks like:

---

![alt text](./images/modify-identity.png)

---

After that we will scroll down, make sure the Target Branch is the one we've generated, and click on the green `commit changes` button.

## Create the Merge Request

Once done, it will redirect us to a new page that will show a bar with a button to generate the desired Merge Request:

---

![alt text](./images/create-mr-after-commit.png)

---

In this page we will be able to modify the name of the Merge Request, and to select the source and target branch where to merge the changes. In this example, we are working on `update/user-affiliation` branch, and our target is `master`:

---

![alt text](./images/mr-submit1.png)

---

Scrolling down we will see all the modifications done in the identities file (commits). We can also click on the tab `Changes` to double check that the modifications done are correct. Finally, we can submit the Merge request:

---

![alt text](./images/mr-submit-2.png)

---

And that's it! This will generate the desired Merge request and will prompt you to a page like this:

---

![alt text](./images/final-mr.png)

---

The Bitergia team will be aware to double check the changes applied, and if everything is correct, it will be merged and applied to the dashboard.
