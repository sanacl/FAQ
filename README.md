Test

User documentation for the [Bitergia] Analytics Platform, powered by the Free Open Source community [GrimoireLab]

## Bitergia Analytics documentation

Welcome data lover!, Bitergia uses Gitlab to help us tracking the status of tickets and some configuration files you may need to make the most out of your dashboard.

The three subsections below cover the **most frequently answered questions**:
1. [How to contact support]
1. [Update the identities and affiliation data]
1. [Updating the list of data sources tracked]

----

The list below contains more documentation aimed to improve your user experience:

1. [Set up your Privacy settings]
1. [Structure of the Gitlab repos]
1. [Set up your Notification preferences]
1. [How to create a Merge Request]

We are working to add more information here to improve your user experience, if
you have any suggestion drop us an email at support at bitergia com


[Bitergia]: http://bitergia.com
[GrimoireLab]: http://grimoirelab.github.io/
[How to contact support]:./gitlab_contact_support/README.md
[Update the identities and affiliation data]:./identities/README.md
[Updating the list of data sources tracked]:./sources/README.md
[Set up your Privacy settings]:./privacy/README.md
[Structure of the Gitlab repos]:./gitlab_repos/README.md
[Set up your Notification preferences]:./gitlab_notification/README.md
[How to create a Merge Request]:./how-to-mr/README.md
